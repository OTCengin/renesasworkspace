
#include "r_cg_userdefine.h"
#include "r_cg_macrodriver.h"
//--------------------------------------------------------------------------------------------------------------

#define TX_BUFFER 	1
#define TX_RX_FIFO	2

#define TX_MODE		TX_BUFFER

#define TX_ID_1		0x11112222
#define TX_ID_2		0x444

#define DATA_LENGTH	8

extern void can_init(void);
extern void can_tx(void);
extern void can0_tx(uint32_t can_id, uint8_t data_length, uint8_t * data_ptr, _Bool isExtended);
extern void can1_tx(uint32_t can_id, uint8_t data_length, uint8_t * data_ptr, _Bool isExtended);

extern unsigned char self_test_done;
extern unsigned char tx_done;
extern unsigned char rx_flag;

typedef enum s_canChannel{
	CAN_CHANNEL_0,
	CAN_CHANNEL_1
} canChannel_t;

typedef struct s_canFrame{
	canChannel_t	channel;
	uint32_t 		ID;
	uint8_t 		DLC;
	uint8_t 		payload[8];
	uint8_t			isExtended;
}canFrame_t;

extern void OTC_CanReception(canFrame_t * frame);