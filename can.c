#include "iodefine.h"
#include "r_cg_userdefine.h"
#include "can.h"
#include "r_cg_macrodriver.h"
//**************************************************************************************************************************

#define true 1
#define false 0

unsigned int i;

uint8_t tx_arr[8] = {0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07};

void can_init(void);
//void can_tx(void);
void can0_tx(uint32_t can_id, uint8_t data_length, uint8_t * data_ptr, _Bool isExtended);
void can1_tx(uint32_t can_id, uint8_t data_length, uint8_t * data_ptr, _Bool isExtended);

unsigned char tx_done;
unsigned char rx_flag;

unsigned char RX_Fifo_empty,RX_fifo_full,RX_Fifo_msg_lost = 0; 

//**************************************************************************************************************************
void can_init(void)
{
	//---------- Set CAN0 port pins -------------------
	R_Pins_Create();

	//Global Status Register
	while((RCFDC0.CFDGSTS.UINT8[LL] && 0x04)==0x04); //wait until CAN RAM is
	                                            //initialized
	                                            //global stop status flag
	//Global Control Register 
	RCFDC0.CFDGCTR.UINT8[LL]      &= 0xFB;   //Transition to global reset mode 
						//from global stop mode
						//GSLPR-->0 other than stop mode
						//GSLPR-->1 stop mode
	
	while(RCFDC0.CFDGSTS.UINT8[LL] != 0x01);	// wait untill "In global reset mode and not in global stop mode
	
						
        //Global Configuration Register
	RCFDC0.CFDGCFG.UINT8[LL] = 0x10;   //xin clock selected
						
	
						
	//Channel Control Register

	// Channel 0
	RCFDC0.CFDC0CTR.UINT8[LL] &= 0xFB;   //Transition to channel reset mode from channel stop mode
	       //CSLPR-->0 other than stop mode
	       //CSLPR-->1 stop mode

	       
	// Channel 1
	RCFDC0.CFDC1CTR.UINT8[LL] &= 0xFB;   //Transition to channel reset mode from channel stop mode
	       //CSLPR-->0 other than stop mode
	       //CSLPR-->1 stop mode
	       
	       
	while((RCFDC0.CFDC0STS.UINT8[LL] && 0x07) != 0x01);	// Wait until transition to channel reset mode

	while((RCFDC0.CFDC1STS.UINT8[LL] && 0x07) != 0x01);	// Wait until transition to channel reset mode
	
	
	
	// Channel 0
	RCFDC0.CFDC0FDCFG.UINT8[HH] |= 0x40;	// Set Classical Mode..... Added by Saurabh Garg

	// Channel 1
	RCFDC0.CFDC1FDCFG.UINT8[HH] |= 0x40;	// Set Classical Mode..... Added by Saurabh Garg
   
//---------- Baud settings for 16MHz clock -------------------------------------
   
   	//Channel 0 settings
	RCFDC0.CFDC0NCFG.UINT32 = 0x04090801;     // 500kbps, 16Tq, 1,4,9, 68.75% sampling, SJW: 1 (=>2)
	
	// Channel 1 settings
   	RCFDC0.CFDC1NCFG.UINT32 = 0x04090801;     // 500kbps, 16Tq, 1,4,9, 68.75% sampling, SJW: 1 (=>2)

//--------- Receive Rule settings for RX FIFO -------------------------------------
	//Receive Rule Configuration Register

	// Channel 0
	RCFDC0.CFDGAFLCFG0.UINT8[HH] = 0x02;    // No. of rules for channel 0 is set to 2
	
	// Channel 1
	RCFDC0.CFDGAFLCFG0.UINT8[HL] = 0x02;    // No. of rules for channel 1 is set to 2
	
	
	RCFDC0.CFDGAFLECTR.UINT8[LH] = 0x01;   // Enable write to receive rule table   
	RCFDC0.CFDGAFLECTR.UINT8[LL] = 0x00;   // receive rule page no.configuration
	
	
// Channnel 0 rules
    // receive rule 1
    
	//Receive Rule ID Register
	RCFDC0.CFDGAFLID0.UINT16[L] = 0x0111;   // Standard, Data frame, 11 bit ID

	//Receive Rule Mask Register
	//RCFDC0.CFDGAFLM0.UINT32 = 0xC00007FF;   // ID bits are compared compared
	RCFDC0.CFDGAFLM0.UINT32 = 0x00000000;	// ID bits are not compared 

	//Receive Rule Pointer 0 Register
	RCFDC0.CFDGAFLP0_0.UINT8[HH] = 0x10;	// Channel 0 rule.... Setting label as 1010
	RCFDC0.CFDGAFLP0_0.UINT8[HL] = 0x10;
	
	
	RCFDC0.CFDGAFLP0_0.UINT8[LH] = 0x00;   // Do not Use messsage buffer as we are using FIFO strategy
	RCFDC0.CFDGAFLP0_0.UINT8[LL] = 0x00;   // DLC Check is disabled.

	//Receive Rule Pointer 1 Register
	RCFDC0.CFDGAFLP1_0.UINT32 = 0x01;   	 // Receive FIFO 0 selected
	

    // receive rule 2
	////Receive Rule ID Register
	RCFDC0.CFDGAFLID1.UINT16[L] = 0x0222;   // Standard, Data frame, 11 bit ID

	//Receive Rule Mask Register
	//RCFDC0.CFDGAFLM1.UINT32 = 0x1FFFFFFF;   // ID bits are compared compared
	RCFDC0.CFDGAFLM1.UINT32 = 0x00000000;	// ID bits are not compared 
	

	//Receive Rule Pointer 0 Register
	RCFDC0.CFDGAFLP0_1.UINT8[HH] = 0x10;	// Channel 0 rule.... Setting label as 1010
	RCFDC0.CFDGAFLP0_1.UINT8[HL] = 0x10;
	
	RCFDC0.CFDGAFLP0_1.UINT8[LH] = 0x00;    //Do not Use messsage buffer as we are using FIFO strategy
	RCFDC0.CFDGAFLP0_1.UINT8[LL] = 0x00;   // DLC Check is disabled.

	//Receive Rule Pointer 1 Register
	RCFDC0.CFDGAFLP1_1.UINT32 = 0x01;       // Receive FIFO 0 selected



//Channel 2 Rx rules
    // receive rule no. 3 (1st for this channel)
    
	//Receive Rule ID Register
	RCFDC0.CFDGAFLID2.UINT16[L] = 0x0333;   // Standard, Data frame, 11 bit ID

	//Receive Rule Mask Register
	//RCFDC0.CFDGAFLM2.UINT32 = 0x1FFFFFFF;   // ID bits are compared compared
	RCFDC0.CFDGAFLM2.UINT32 = 0x00000000;	// ID bits are not compared 

	//Receive Rule Pointer 0 Register
	
	RCFDC0.CFDGAFLP0_2.UINT8[HH] = 0x20;	// Channel 1 rule.... Setting label as 2020
	RCFDC0.CFDGAFLP0_2.UINT8[HL] = 0x20;
	
	RCFDC0.CFDGAFLP0_2.UINT8[LH] = 0x00;   // Do not Use messsage buffer as we are using FIFO strategy
	RCFDC0.CFDGAFLP0_2.UINT8[LL] = 0x00;   // DLC Check is disabled.

	//Receive Rule Pointer 1 Register
	RCFDC0.CFDGAFLP1_2.UINT32 = 0x01;   	 // Receive FIFO 0 selected


    // receive rule 2 (2nd for this channel)
	////Receive Rule ID Register
	RCFDC0.CFDGAFLID3.UINT16[L] = 0x0444;   // Standard, Data frame, 11 bit ID

	//Receive Rule Mask Register
	//RCFDC0.CFDGAFLM3.UINT32 = 0x1FFFFFFF;   // ID bits are compared compared
	RCFDC0.CFDGAFLM3.UINT32 = 0x00000000;	// ID bits are not compared 

	//Receive Rule Pointer 0 Register
	RCFDC0.CFDGAFLP0_3.UINT8[HH] = 0x20;	// Channel 1 rule.... Setting label as 2020
	RCFDC0.CFDGAFLP0_3.UINT8[HL] = 0x20;

	RCFDC0.CFDGAFLP0_3.UINT8[LH] = 0x00;    //Do not Use messsage buffer as we are using FIFO strategy
	RCFDC0.CFDGAFLP0_3.UINT8[LL] = 0x00;   // DLC Check is disabled.

	//Receive Rule Pointer 1 Register
	RCFDC0.CFDGAFLP1_3.UINT32 = 0x01;       // Receive FIFO 0 selected


	//End of Receive Rule....


	//Receive rule entry control register
	RCFDC0.CFDGAFLECTR.UINT8[LH] = 0x00; // Disable write to receive rule table

	RCFDC0.CFDRFCC0.UINT16[L] = 0x1202;  // RFIGCV = xxx(don't care),RFIM = 1(An interrupt occurs each time a message has been received.)
	                        // RFDC =0x010 Receive FIFO Buffer Depth Configuration => 8
	      			// RFPLS = 0x000 Receive FIFO Buffer Payload Storage Size Select is 8 bytes
	      			// RFIE =1 Receive FIFO Interrupt Enable
	// End of Buffer Setting                 

//----------- Global Interrupt Settings -------------------------------------
   
     RCFDC0.CFDGCTR.UINT8[LH] = 0x00;      // No Error Interrupts Enabled.
   
//------------ Channel Interrupt setting -----------------------------------

//No channel Interrupts used.

//------------ Interrupt Controller Unit ------------------------------------

   //CAN Receive FIFO Interrupt.
   INTC1.ICRCANGRECC0.BIT.TBRCANGRECC0 = 1;
   INTC1.ICRCANGRECC0.BIT.MKRCANGRECC0 = 0;
   INTC1.ICRCANGRECC0.BIT.RFRCANGRECC0 = 0;
      
   
//------------ Operating mode --------------------------------------
   
   RCFDC0.CFDGCTR.UINT8[LL] = 0x00;      //Other than global stop mode
                                           //Global operating mode
   for(i=0;i<0xfff;i++);   //wait for transistion
        
   //Set RFE bit in global operating mode 
   RCFDC0.CFDRFCC0.UINT8[LL] |= 0x01;      // receive FIFO is used

   
   RCFDC0.CFDC0CTR.UINT8[LL] = 0x00;   //other than channel stop mode
                                           //channel communication mode

   RCFDC0.CFDC1CTR.UINT8[LL] = 0x00;   //other than channel stop mode
                                           //channel communication mode
					   
   for(i=0;i<0xfff;i++);   //wait for transistion   
}

//**************************************************************************************************************************

void can0_tx(uint32_t can_id, uint8_t data_length, uint8_t *data_ptr, _Bool isExtended)
{
//----------- transmit using transmit buffer on channel 0 ---------------------------------------------   

   if(!(RCFDC0.CFDTMSTS0 & 0x01))	
   {       
	RCFDC0.CFDTMSTS0 = 0x00;
	
	RCFDC0.CFDTMIEC0.UINT8[0] = 0x01;		// Transmit Buffer 0 interrupt enabled
	
	if (isExtended == true)
	{
	  	//Extended ID
		//Transmit buffer ID register
	      	RCFDC0.CFDTMID0.UINT32 = can_id + 0x80000000;   // Transmit ID, Extended data frame, 
	}
	else if(isExtended == false)
	{
	  	//Extended ID
		//Transmit buffer ID register
	      	RCFDC0.CFDTMID0.UINT32 = can_id;   // Transmit ID, standard data frame, 
	}
	
      	
	//Transmit buffer pointer register
      	RCFDC0.CFDTMPTR0.UINT8[HH] = data_length << 4;   // DLC: 8 data bytes.
      	
      	RCFDC0.CFDTMFDCTR0.UINT32 = 0x00;

	//Transmit buffer data field register
      	//RCFDC0.CFDTMDF0_32.UINT32 = 0x55555555;   // data bytes 0 to 3
	RCFDC0.CFDTMDF0_0.UINT8[HH] = (*data_ptr);//(*data_ptr);   data[0];
	RCFDC0.CFDTMDF0_0.UINT8[HL] = (*data_ptr+1);//(*data_ptr+1); data[1];
	RCFDC0.CFDTMDF0_0.UINT8[LH] = (*data_ptr+2);//(*data_ptr+2); data[2];
	RCFDC0.CFDTMDF0_0.UINT8[LL] = (*data_ptr+3);  //(*data_ptr+3); data[3];  // data bytes 0 to 3
     
      	//RCFDC0.CFDTMDF1_32.UINT32 = 0x55555555;   // data bytes 4 to 7
	RCFDC0.CFDTMDF1_0.UINT8[HH] = (*data_ptr+4);//(*data_ptr); data[0];
	RCFDC0.CFDTMDF1_0.UINT8[HL] = (*data_ptr+5);//(*data_ptr+1); data[1];
	RCFDC0.CFDTMDF1_0.UINT8[LH] = (*data_ptr+6);//(*data_ptr+2); data[2];
	RCFDC0.CFDTMDF1_0.UINT8[LL] = (*data_ptr+7);//(*data_ptr+3); data[3];  // data bytes 0 to 3

	//Transmit buffer status register
      	RCFDC0.CFDTMC0 = 0x01;         // Transmission requested - TMTR bit
   }
}

void can1_tx(uint32_t can_id, uint8_t data_length, uint8_t * data_ptr, _Bool isExtended)
{
//----------- transmit using transmit buffer on channel 1 ---------------------------------------------

   if(!(RCFDC0.CFDTMSTS32 & 0x01))	
   {       
      	RCFDC0.CFDTMSTS32 = 0x00;
	
	RCFDC0.CFDTMIEC1.UINT8[0] = 0x01;		// Transmit Buffer 0 interrupt enabled
	
	if (isExtended == true)
	{
	  	//Extended ID
		//Transmit buffer ID register
	      	RCFDC0.CFDTMID32.UINT32 = can_id + 0x80000000;   // Transmit ID, Extended data frame, 
	}
	else if(isExtended == false)
	{
	  	//Extended ID
		//Transmit buffer ID register
	      	RCFDC0.CFDTMID32.UINT32 = can_id;   // Transmit ID, standard data frame, 
	}
      	
	//Transmit buffer pointer register
      	RCFDC0.CFDTMPTR32.UINT8[HH] = data_length << 4;   // DLC: 8 data bytes.
      	
      	RCFDC0.CFDTMFDCTR32.UINT32 = 0x00;

	//Transmit buffer data field register
      	//RCFDC0.CFDTMDF0_50.UINT32 = 0xaaaaaaaa;   // data bytes 0 to 3
	RCFDC0.CFDTMDF0_32.UINT8[HH] = (*data_ptr);
	RCFDC0.CFDTMDF0_32.UINT8[HL] = (*data_ptr+1);
	RCFDC0.CFDTMDF0_32.UINT8[LH] = (*data_ptr+2);
	RCFDC0.CFDTMDF0_32.UINT8[LL] = (*data_ptr+3);   // data bytes 0 to 3
      
      	//RCFDC0.CFDTMDF1_50.UINT32 = 0xaaaaaaaa;   // data bytes 4 to 7
	RCFDC0.CFDTMDF1_32.UINT8[HH] = (*data_ptr+4);
	RCFDC0.CFDTMDF1_32.UINT8[HL] = (*data_ptr+5);
	RCFDC0.CFDTMDF1_32.UINT8[LH] = (*data_ptr+6);
  RCFDC0.CFDTMDF1_32.UINT8[LL] = (*data_ptr+7);   // data bytes 4 to 7
      
	//Transmit buffer status register
      	RCFDC0.CFDTMC32 = 0x01;         // Transmission requested - TMTR bit
   }
}

//**************************************************************************************************************************
//unsigned int rx_msg_Label_info;
//unsigned char k;
//unsigned char m;


//typedef struct 
//{
//unsigned char rx_msg_DLC;
//unsigned int rx_msg_ID;
//unsigned long rx_msg_data0;   
//unsigned long rx_msg_data1;
//unsigned int rx_msg_Label_info;

//}RX_fifo_data;

// RX_fifo_data Recd_RX_Channel0_arr[100];
// RX_fifo_data Recd_RX_Channel1_arr[100];

//**************************************************************************************************************************

void my_can0_rx(void)
{
	canFrame_t frame;
//-------------------- Rx by reading Rx FIFO buffer-------------------------------------------------------   
   RCFDC0.CFDRFSTS0.UINT8[0] &= 0xF7;   // CLEAR RFIF flag
   if((RCFDC0.CFDRFSTS0.UINT8[0] & 0x01) != 0x01)
   {
      do
      {
				 frame.channel = (RCFDC0.CFDRFFDSTS0.UINT32 >> 16) == 0x1010 ? CAN_CHANNEL_0 : CAN_CHANNEL_1;
				 frame.ID = RCFDC0.CFDRFID0.UINT32&0x7FFFFFFF;
				 frame.isExtended = (uint8_t)((RCFDC0.CFDRFID0.UINT32&0x80000000) >> 31);
				 frame.DLC = (RCFDC0.CFDRFPTR0.UINT8[3] & 0xf0) >> 4;
				 frame.payload[0] = RCFDC0.CFDRFDF0_0.UINT8[LL];
				 frame.payload[1] = RCFDC0.CFDRFDF0_0.UINT8[LH];
				 frame.payload[2] = RCFDC0.CFDRFDF0_0.UINT8[HL];
				 frame.payload[3] = RCFDC0.CFDRFDF0_0.UINT8[HH];
				 frame.payload[4] = RCFDC0.CFDRFDF1_0.UINT8[LL];
				 frame.payload[5] = RCFDC0.CFDRFDF1_0.UINT8[LH];
				 frame.payload[6] = RCFDC0.CFDRFDF1_0.UINT8[HL];
				 frame.payload[7] = RCFDC0.CFDRFDF1_0.UINT8[HH];
				 OTC_CanReception(&frame);
				 
//	 if(rx_msg_Label_info == 0x1010)
//	 {
//	 	// then this packet is on channel 0
//		// channel 0 storing variables
        	
//			Recd_RX_Channel0_arr[k].rx_msg_ID = RCFDC0.CFDRFID0.UINT32;
//         	Recd_RX_Channel0_arr[k].rx_msg_DLC = (RCFDC0.CFDRFPTR0.UINT8[3] & 0xf0) >> 4;
         
//         	Recd_RX_Channel0_arr[k].rx_msg_data0 = RCFDC0.CFDRFDF0_0.UINT32;
//         	Recd_RX_Channel0_arr[k].rx_msg_data1 = RCFDC0.CFDRFDF1_0.UINT32;  
//	 	Recd_RX_Channel0_arr[k].rx_msg_Label_info = (RCFDC0.CFDRFFDSTS0.UINT32 >> 16);	

//		k++;
//		if(k>100)
//		{
//			k=0;
//		}
//	 }
//	 else if(rx_msg_Label_info == 0x2020)
//	 {
//		 // then this packet is on channel 1
//	 	// channel 1 storing variables.
		
//		Recd_RX_Channel1_arr[m].rx_msg_ID = RCFDC0.CFDRFID0.UINT32;
//         	Recd_RX_Channel1_arr[m].rx_msg_DLC = (RCFDC0.CFDRFPTR0.UINT8[3] & 0xf0) >> 4;
         
//         	Recd_RX_Channel1_arr[m].rx_msg_data0 = RCFDC0.CFDRFDF0_0.UINT32;
//         	Recd_RX_Channel1_arr[m].rx_msg_data1 = RCFDC0.CFDRFDF1_0.UINT32;  
//	 	Recd_RX_Channel1_arr[m].rx_msg_Label_info = (RCFDC0.CFDRFFDSTS0.UINT32 >> 16);
		
//		m++;
//		if(m>100)
//		{
//			m=0;
//		}
//	 }

	 RCFDC0.CFDRFPCTR0.UINT8[0] = 0xFF;
      }while((RCFDC0.CFDRFSTS0.UINT8[0] & 0x01) != 0x01);// check if FIFO empty
   }
//--------------------------------------------------------------------------------------------------------
}

//**************************************************************************************************************************

#pragma interrupt CAN_Rx_FIFO_ISR(enable=false, channel=23, fpu=true, callt=false)
void CAN_Rx_FIFO_ISR(void)
{
     __nop();
     rx_flag = 1;
     RCFDC0.CFDRFSTS0.UINT8[0] &= 0xF7;   // CLEAR RFIF flag

     if(RCFDC0.CFDRFSTS0.UINT8[0] & 0x01)  	// check if FIFO empty
     {
             RX_Fifo_empty = 1;   
        return;
     }
     else if(RCFDC0.CFDRFSTS0.UINT8[0] & 0x02) // check if FIFO full
     {
        RX_fifo_full = 1;     
     }
     my_can0_rx();
}

//**************************************************************************************************************************

#pragma interrupt CAN0_Tx_ISR(enable=false, channel=26, fpu=true, callt=false)
void CAN0_Tx_ISR(void)
{

#if TX_MODE == TX_BUFFER   
   RCFDC0.CFDTMSTS0 = 0x00;
   __nop();
#else                  			// TX_RX_FIFO
   RCFDC0.CFDCFSTS0.UINT8[0] &= 0xE7;   //clear interrupt flags
#endif
   INTC1.ICRCAN0TRX.BIT.RFRCAN0TRX = 0;
   __nop();
   tx_done = 1;
     
}

//**************************************************************************************************************************

#pragma interrupt CAN1_Tx_ISR(enable=false, channel=115, fpu=true, callt=false)
void CAN1_Tx_ISR(void)
{

#if TX_MODE == TX_BUFFER   
   RCFDC0.CFDTMSTS0 = 0x00;
   __nop();
#else                  			// TX_RX_FIFO
   RCFDC0.CFDCFSTS0.UINT8[0] &= 0xE7;   //clear interrupt flags
#endif
   INTC1.ICRCAN0TRX.BIT.RFRCAN0TRX = 0;
   __nop();
   tx_done = 1;
     
}
